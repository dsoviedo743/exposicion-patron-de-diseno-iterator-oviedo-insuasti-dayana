﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator_
{
    public class IteratorEjecutador
    {
        //Pasa como parametro el objeto de tipo Ejecutador
        private ArrayList ejecutador;
        private int posicion;

        //Clase"IteratorEjecutador" es una Arraylist con dos variables privadas
        public IteratorEjecutador(Ejecutador o)
        {
            this.ejecutador = o.Datos;
           posicion = 0;
        }

        //metodos validacion. 
        public Boolean ExisteSiguiente()
        {
            if (posicion < ejecutador.Count)
                return true;
            else
                return false;
        }
        public Object Siguiente()
        {
            //Va obteniendo de manera alternativa el siguiente elemento de la lista del encuentro al que le interesa
            object valor = ejecutador[posicion];
            posicion++;
            return valor;
        }
    }
}
