﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator_
{
    class Program
    {
        static void Main(string[] args)
        {
            //Objeto de tipo Ejecutador al que le hemos añadido el nombre de Ejecutador 
            //Tenemos una Arraylist de 4 objetos 
            Ejecutador lista = new Ejecutador();
            lista.addvalor("Proceso 1 ");
            lista.addvalor("Proceso 2 ");
            lista.addvalor("Proceso 3 ");
            lista.addvalor("Proceso 4");

            IteratorEjecutador Iterador = lista.Iterator();
            //Recorrido con el Iterator 
            //Metodo "ExisteSiguiente valida si existe el siguiente y en el caso de existir el siguiente lo imprime.
            while (Iterador.ExisteSiguiente())
            Console.WriteLine(Iterador.Siguiente());
            Console.ReadLine();

        }
    }
}
