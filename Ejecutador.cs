﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator_
{     
    
    public class Ejecutador
    {
        //Arraylist contenedra objetos de tipo string 
        public ArrayList Datos;
        private Ejecutador o;

        //Constructor de instancia datos de la Arraylist
        public Ejecutador()
        {
            Datos = new ArrayList();
        }
        //Metodo dameValor indicara el valor que hay en la posicion de la lista
        public Ejecutador dameValor(int pos)
        {
            Ejecutador o= null;
            //Si la posicion que se pase como parametro es menor que un elemento de la lista pues nos devuelve el elemento a esa posicion en 
            if (pos < Datos.Count)
                return (Ejecutador)Datos[pos];
            return o;
        } //Elemento para añadir un valor a la lista "Addvalor"
        public void addvalor(String valor)
        {
            Datos.Add(valor);
        }//Tenemos un metodo "Tamaño" que nos da la dimension de la Arraylist 
        public int Tamaño()
        {
            return Datos.Count;
        } 
        //Metodo publico que nos devuelve un objeto de tipo Iterator Ejecutador 
        public IteratorEjecutador Iterator()
        {
            return new IteratorEjecutador(this);
        }
        
        

    }
}
